// pass it the top right point of the cell u want to move to
var _x = argument[0];
var _y = argument[1];
var prevX = x;
var prevY = y;
// Am i a balloon? you can push me into spikes
var num_cells_to_check_x = round(sprite_width / global.CELL_SIZE);
var num_cells_to_check_y = round(sprite_height / global.CELL_SIZE);
var i;
var k;
var inst = noone;
if (object_index == obj_balloon || object_index == obj_balloon_blue || object_index == obj_balloon_green) {
    for (i = 0; i < num_cells_to_check_x; ++i) {
        var x_offset = global.CELL_SIZE * i;
        for (k = 0; k < num_cells_to_check_y; ++k) {
            var y_offset = global.CELL_SIZE * k;
            inst = instance_position(_x+x_offset+global.CELL_SIZE/2, _y+y_offset+global.CELL_SIZE/2, obj_spike);
            if (inst != noone) {
                script_execute(pop_balloon, self);
                return true;
            }
        }
    }
}

// check if there is a balloon there
x += 999;
for (i = 0; i < num_cells_to_check_x; ++i) {
    var x_offset = global.CELL_SIZE * i;
    for (k = 0; k < num_cells_to_check_y; ++k) {
        var y_offset = global.CELL_SIZE * k;
        inst = instance_position(_x+x_offset+global.CELL_SIZE/2, _y+y_offset+global.CELL_SIZE/2, obj_balloon);
        if (inst != noone)
            break;
    }
    if (inst != noone)
        break;
}
x -= 999;
if (inst != noone) {
    // check if balloon can be moved
    with (inst)
        if (script_execute(can_move_to, x+(_x-prevX), y+(_y-prevY))) {
            x += _x-prevX;
            y += _y-prevY;
        } else {
            return false;
        }
}

x += 999;
for (i = 0; i < num_cells_to_check_x; ++i) {
    var x_offset = global.CELL_SIZE * i;
    for (k = 0; k < num_cells_to_check_y; ++k) {
        var y_offset = global.CELL_SIZE * k;
        if (position_meeting(_x+x_offset+global.CELL_SIZE/2, _y+y_offset+global.CELL_SIZE/2, obj_blocking)) {
                x -= 999;
                return false;
        }
    }
}
x -= 999;
return true;
