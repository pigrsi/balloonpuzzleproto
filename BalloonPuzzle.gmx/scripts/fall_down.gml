// every step check if there is ground
var inst = instance_place(x, y+1, obj_blocking);
if (inst != noone) {
    y = inst.y - sprite_height;
    gravity = 0;
    vspeed = 0;
    // If it's a balloon, push it down
    if (!pushing_balloon_down && (inst.object_index == obj_balloon || inst.object_index == obj_balloon_blue || inst.object_index == obj_balloon_green)) {
        balloon_pushing = inst;
        pushing_balloon_down = true;
        pushing_balloon_delay = 0;
        script_execute(push_balloon_down);
    }
}
