// called by player when he lands on a balloon
if (pushing_balloon_delay > 0) {
    pushing_balloon_delay -= 1/room_speed;
    exit;
}
var balloon = balloon_pushing;
if (fall_power <= 0)
    fall_power = round(y) - round(last_standing_y);
var min_required = global.CELL_SIZE/2;
var suck_size = global.CELL_SIZE;
if (fall_power > min_required) {
    with (balloon)
        var can_move = script_execute(can_move_to, balloon.x, balloon.y+32);
    if (can_move) {
        if (!instance_exists(balloon))
            script_execute(player_jump);
        else {
            balloon.y += 32;
            y = balloon.y - sprite_height;
        }
    } else {
        if (fall_power > 80) {
            script_execute(pop_balloon, balloon);
            script_execute(player_jump);
            fall_power = 0;
            balloon_pushing = noone;
            pushing_balloon_down = false;
            pushing_balloon_delay = 0;
            gravity = player_gravity;
            vspeed = 0;
        }
    }
    fall_power -= suck_size;
    pushing_balloon_delay = 0.15;
} else {
    fall_power = 0;
    balloon_pushing = noone;
    pushing_balloon_down = false;
    pushing_balloon_delay = 0;
}
// At the end
last_standing_y = y;
