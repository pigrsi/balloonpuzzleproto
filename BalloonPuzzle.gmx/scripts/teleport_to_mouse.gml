if mouse_check_button(mb_right) {
    var remainder = round(mouse_x) % 32;
    if (remainder > 16) {
        x = round(mouse_x) + (32-remainder);
    } else {
        x = round(mouse_x) - remainder;
    }

    var remainder = round(mouse_y) % 32;
    if (remainder > 16) {
        y = round(mouse_y) + (32-remainder);
    } else {
        y = round(mouse_y) - remainder;
    }
    
    last_standing_y = y;
    vspeed = 0;
    gravity = player_gravity;
    move_delay = 0.1;
}
