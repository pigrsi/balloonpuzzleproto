if (move_delay >= 0) {
    move_delay -= 1/room_speed;
    exit;
}
if (gravity != 0 || vspeed != 0)
    exit;
    
if (pushing_balloon_down && balloon_pushing != noone) {
    if (balloon_pushing == noone || !instance_exists(balloon_pushing)) {
        pushing_balloon_down = false;
        exit;
    }
    script_execute(push_balloon_down, balloon_pushing);
    exit;
}

if (keyboard_check(vk_left)) {
    if (script_execute(can_move_to, x-32, y)) {
        x -= 32;
        gravity = player_gravity;
        move_delay = 0.1;
    }
} else if (keyboard_check(vk_right)) {
   if (script_execute(can_move_to, x+32, y)) {
        x += 32; 
        gravity = player_gravity;
        move_delay = 0.1;
   }
} else if (keyboard_check(vk_up)) {
    script_execute(player_jump);
} // For these, move up and then to the side in one go, but there must be something to climb on
else if (keyboard_check(ord('A'))) {
    if (script_execute(can_move_to, x, y-32)) {
        y -= 32; 
        vspeed = 0;
        gravity = player_gravity;
        last_standing_y = y;
        // Something to climb on
        if (position_meeting(x-global.CELL_SIZE/2, y+32, obj_blocking)) {
            // Try to move left now
            if (script_execute(can_move_to, x-32, y)) {
                x -= 32;
                move_delay = 0.1;
            }
        }
        
    }
} else if (keyboard_check(ord('D'))) {
    if (script_execute(can_move_to, x, y-32)) {
        y -= 32; 
        vspeed = 0;
        gravity = player_gravity;
        last_standing_y = y;
        // Something to climb on
        if (position_meeting(x+sprite_width+global.CELL_SIZE/2, y+32, obj_blocking)) {
            // Try to move right now
            if (script_execute(can_move_to, x+32, y)) {
                x += 32;
                move_delay = 0.1;
            }
        }
    }
}
