if (script_execute(can_move_to, x, y-32)) {
    y -= 32; 
    vspeed = 0;
    gravity = player_gravity;
    last_standing_y = y;
    move_delay = 0.1;
}
